import 'package:dio/dio.dart';

abstract class ApiProvider<T> {
  Future<T> getData();
}

class ApiProviderImpl implements ApiProvider {
  final Dio _dio = Dio();

  @override
  Future getData() async {
    try {
      Response response = await _dio.get(
        'https://us-central1-gettheemailtest.cloudfunctions.net/start',
        options: Options(
          headers: {'Authorization': 'sim-sim'},
        ),
      );
      if (response.statusCode == 200) {
        return response.data;
      }
    } on Exception catch (e) {
      return e.toString();
    }
  }
}
