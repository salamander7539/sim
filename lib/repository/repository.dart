import 'package:test_app/api_provider/api_provider.dart';

abstract class Repository<T> {
  Future<T> getString();
}

class RepositoryImpl implements Repository {
  final ApiProvider? apiProvider;

  RepositoryImpl({required this.apiProvider});

  @override
  Future getString() async {
    // TODO: implement rating
    final String text = await apiProvider?.getData();
    return text;
  }
}
