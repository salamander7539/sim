import 'package:flutter/material.dart';
import 'package:test_app/Screens/string_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (_) => StringScreen()));
          },
          child: Text('Next'),
        ),
      ),
    );
    ;
  }
}
