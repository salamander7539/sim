import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/Screens/string_screen_view.dart';
import 'package:test_app/bloc/test_bloc.dart';
import 'package:test_app/injections/repository_di.dart';

class StringScreen extends StatefulWidget {
  const StringScreen({Key? key}) : super(key: key);

  @override
  _StringScreenState createState() => _StringScreenState();
}

class _StringScreenState extends State<StringScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<TestBlocImpl>(
      create: (context) => TestBlocImpl(repository: RepositoryInject.repository()),
      child: StringScreenView(),
    );
  }
}
