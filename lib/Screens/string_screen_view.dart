import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/bloc/test_bloc.dart';

class StringScreenView extends StatefulWidget {
  const StringScreenView({Key? key}) : super(key: key);

  @override
  _StringScreenViewState createState() => _StringScreenViewState();
}

class _StringScreenViewState extends State<StringScreenView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<TestBlocImpl>(context).add(GetDataEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocBuilder<TestBlocImpl, TestState>(
          builder: (context, state) {
            if (state is TestStringLoadedState) {
              return Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: AutoSizeText(
                  state.text,
                  style: TextStyle(fontSize: 40.0),
                ),
              );
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
