import 'package:test_app/api_provider/api_provider.dart';
import 'package:test_app/injections/api_provider_di.dart';
import 'package:test_app/repository/repository.dart';

class RepositoryInject {
  static Repository? _repository;

  RepositoryInject._();

  static Repository? repository() {
    if (_repository == null) {
      _repository = RepositoryImpl(apiProvider: ApiProviderInject.apiProvider());
    }
    return _repository;
  }
}
