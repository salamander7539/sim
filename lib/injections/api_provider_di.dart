import 'package:test_app/api_provider/api_provider.dart';

class ApiProviderInject {
  static ApiProvider? _apiProvider;

  ApiProviderInject._();

  static ApiProvider? apiProvider() {
    if (_apiProvider == null) {
      _apiProvider = ApiProviderImpl();
    }
    return _apiProvider;
  }
}