part of 'test_bloc.dart';

abstract class TestState extends Equatable {
  const TestState();
}

class TestInitial extends TestState {
  @override
  List<Object> get props => [];
}

class TestStringLoadedState extends TestState {
  final String text;

  const TestStringLoadedState({required this.text});

  @override
  List<Object> get props => [text];
}

class ErrorState extends TestState {
  final String error;

  ErrorState({required this.error});

  @override
  List<Object> get props => [error];
}
