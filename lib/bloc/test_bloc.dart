import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:test_app/repository/repository.dart';

part 'test_event.dart';

part 'test_state.dart';

abstract class TestBloc extends Bloc<TestEvent, TestState> {
  TestBloc(TestState initialState) : super(initialState);
}

class TestBlocImpl extends Bloc<TestEvent, TestState> {
  final Repository? repository;

  TestBlocImpl({required this.repository}) : super(TestInitial());

  @override
  Stream<TestState> mapEventToState(TestEvent event) async* {
    // TODO: implement mapEventToState
    if (event is GetDataEvent) {
      final String text = await repository?.getString();
      yield TestStringLoadedState(text: text);
    }
  }
}
